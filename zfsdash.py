# Copyright 2012 Ben Kinslow
#    
# This file is part of ZFSDash.
#
# ZFSDash is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# ZFSDash is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with ZFSDash.  If not, see <http://www.gnu.org/licenses/>
    
import cherrypy
import ipaddresses
import settings
import zpool_manager
import logging
import logging.handlers
import webber
import os
import sys


from cherrypy.process import plugins

poolmanager = zpool_manager.ZpoolManager()
ipAddresses = ipaddresses.IpAddresses()

class ZFSDash(object):

    def __init__(self):        
        logging.basicConfig(filename=settings.Logging.fileName, level=settings.Logging.level)
        rotatingFileHandler = logging.handlers.RotatingFileHandler(__name__, maxBytes=5120, backupCount=0)
        
        PoolManagerFeature(cherrypy.engine).subscribe()
        IpCheckerFeature(cherrypy.engine).subscribe()

    @cherrypy.expose
    def shutdown(self):
        sys.exit()

    @cherrypy.expose
    def index(self):
        return self.pools()

    @cherrypy.expose
    def pools(self):
        if poolmanager != None:
            poolmanager.updatePools()
            pageText = webber.getPools(poolmanager.pools)
        else:
            pageText = 'Error accessing zpools'
        return pageText
    
    @cherrypy.expose
    def startScrub(self, poolName):
        zpoolCommand = 'zpool scrub ' + poolName
        zpool_manager.Zpool.executeZpoolCommand(zpoolCommand, "")
        raise cherrypy.HTTPRedirect('pools') 
           
class PoolManagerFeature(plugins.SimplePlugin):

    def start(self):
        logging.info("Starting pool manager")
        poolmanager.startThreads()

    def stop(self):
        logging.info("Stopping pool manager.")
        poolmanager.stopThreads()

class IpCheckerFeature(plugins.SimplePlugin):

    def start(self):
        logging.info("Starting IP Checker")
        ipAddresses.startChecker()

    def stop(self):
        logging.info("Stopping IP Checker.")
        ipAddresses.stopChecker()

current_dir = os.path.dirname(os.path.abspath(__file__))
# Set up site-wide config first so we get a log if errors occur.
cherrypy.config.update({'tools.staticdir.root': os.path.join(os.path.abspath(current_dir), 'trunk/root')})

cherrypy.server.socket_host = settings.Web.socket_host
cherrypy.server.socket_port = settings.Web.socket_port
cherrypy.quickstart(ZFSDash())
