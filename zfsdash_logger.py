# Copyright 2012 Ben Kinslow
#    
# This file is part of ZFSDash.
#
# ZFSDash is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# ZFSDash is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with ZFSDash.  If not, see <http://www.gnu.org/licenses/>
    
import logging
import logging.handlers
import settings

logging.basicConfig(filename=settings.Logging.fileName, level=settings.Logging.level)

zfsDashLogger = logging.getLogger(__name__)

rotatingFileHandler = logging.handlers.RotatingFileHandler(__name__, maxBytes=5120, backupCount=0)
zfsDashLogger.addHandler(rotatingFileHandler)

def getLogger():
	return zfsDashLogger
        
        
