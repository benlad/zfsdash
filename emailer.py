# Copyright 2012 Ben Kinslow
#    
# This file is part of ZFSDash.
#
# ZFSDash is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# ZFSDash is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with ZFSDash.  If not, see <http://www.gnu.org/licenses/>

import smtplib
import settings
import webber

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

emailSettings = settings.Email()

def sendSimpleEmail(subject, body):
 
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = emailSettings.fromAddr
    msg['To'] = emailSettings.toAddr
    
    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(body, 'plain')
    
    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)

    s = smtplib.SMTP(emailSettings.smtpServer)
    s.ehlo()
    s.starttls()
    s.login(emailSettings.username, emailSettings.password)  
                
    s.sendmail(emailSettings.fromAddr, emailSettings.toAddr, msg.as_string())
    s.quit()
    
    return s  

def sendEmail(pools, warning=False):

    textPoolSummary = ''.join(str(pool) for pool in pools)
    htmlPoolSummary = webber.getPools(pools, True)
    poolNames = ', '.join(pool.name for pool in pools)

    subject = "ZFS Pools: " + poolNames
    if warning:
        subject = "WARNING CHECK POOL STATE - " + subject
    
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = emailSettings.fromAddr
    msg['To'] = emailSettings.toAddr
    
    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(textPoolSummary, 'plain')
    part2 = MIMEText(htmlPoolSummary, 'html')
    
    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    s = smtplib.SMTP(emailSettings.smtpServer)
    s.ehlo()
    s.starttls()
    s.login(emailSettings.username, emailSettings.password)  
                
    s.sendmail(emailSettings.fromAddr, emailSettings.toAddr, msg.as_string())
    s.quit()
    
    return s      