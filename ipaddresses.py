# Copyright 2012 Ben Kinslow
#    
# This file is part of ZFSDash.
#
# ZFSDash is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# ZFSDash is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with ZFSDash.  If not, see <http://www.gnu.org/licenses/>

import datetime
import emailer
import logging
import scheduler
import urllib
import re
import socket

class IpAddresses():
    
    def __init__(self): 
        self.ipAddress = ''
		
    def startChecker(self):
        self.ipCheckerSchedue = scheduler.Scheduler(self.checkIpAddress, period=datetime.timedelta(minutes=15), runNow=True, threadName="Ip Address Checker")

    def stopChecker(self):
        self.ipCheckerSchedue.abort = True

    def checkIpAddress(self):
        currentIpAddress = self.getPublicIpaddress()
        #logging.info("Check Ip - Current:[" + self.ipAddress + "]   New:[" + currentIpAddress + "]")
        if (currentIpAddress != self.ipAddress):
            subject = "New IP Address" if self.ipAddress != '' else 'ZFS Dash Starting'
            self.ipAddress = currentIpAddress
            emailer.sendSimpleEmail(subject, socket.gethostname() + ": " + self.ipAddress)

    def getPublicIpaddress(self):
        data = str(urllib.urlopen('http://checkip.dyndns.com/').read())
        # data = '<html><head><title>Current IP Check</title></head><body>Current IP Address: 65.96.168.198</body></html>\r\n'
        result = re.compile(r'Address: (\d+\.\d+\.\d+\.\d+)').search(data).group(1)
        return result


