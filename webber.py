# Copyright 2012 Ben Kinslow
#    
# This file is part of ZFSDash.
#
# ZFSDash is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# ZFSDash is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with ZFSDash.  If not, see <http://www.gnu.org/licenses/>

import logging
import zpool_manager

from mako.template import Template
from mako.lookup import TemplateLookup
from mako.runtime import supports_caller

from GChartWrapper import *

mako_lookup = TemplateLookup(directories = ['ui'], 
                                  module_directory = 'ui/mako_modules', 
                                  collection_size = 50)

        
def getPools(pools, forEmail=False):
    poolsTemplate = mako_lookup.get_template("pools.html")
    return poolsTemplate.render(title='Pools', pools=pools, forEmail=forEmail)

def getChart(pool):
    usedColour = 'lime'
    actionColour = 'olive'
    if pool.health == pool.Health.DEGRADED:
        usedColour = 'orange'
    elif pool.health != pool.Health.ONLINE:
        usedColour = 'red'

    used = pool.capacity
    empty = 100 - used
    
    if pool.scrubbing:
        scrubbedAllocatedPercent = used * pool.scrubPercent / 100.0
        toScrubAllocatedPercent = used - scrubbedAllocatedPercent
        
        scrubbedKey = 'Scrubbed - ' + '%.2f' % (pool.allocatedGigaBytes * pool.scrubPercent / 100.0) + 'G'
        toScrubKey = 'To Scrub - ' + '%.2f' % (pool.allocatedGigaBytes * (1.0 - pool.scrubPercent / 100.0)) + 'G'
        
        pieChartAllocated = Pie3D([empty, toScrubAllocatedPercent, scrubbedAllocatedPercent]).color('black', actionColour, usedColour)
        pieChartAllocated.label('Free - ' + pool.freeStr, toScrubKey, scrubbedKey)
        pieChartAllocated.size([500, 220])
        pieChartAllocated.margin(110, 110, 5, 5)
    else:
        pieChartAllocated = Pie3D([empty, used]).color('black', usedColour)
        pieChartAllocated.label('Free - ' + pool.freeStr, 'Used - ' + pool.allocatedStr)
        pieChartAllocated.size([450, 200])      
        pieChartAllocated.margin(10, 10, 5, 5)

    string = pieChartAllocated.img(id="pieChartAllocated")
    logging.debug("Returning zpool chart: " + string)
    return string
    
def getButton(action, caption, enabled = True):
    form = '<form action="' + action + '" method="post">'
    form += '<input type="submit" value="' + caption + '" ' + ('' if enabled else 'disabled') + '/>'
    form += "</form>"
    return form
    
def getButtonWithParam(action, caption, paramName = '', paramValue = '', enabled = True):
    form = '<form action="' + action + '" method="post">'
    form += '<input type="submit" value="' + caption + '" ' + ('' if enabled else 'disabled') + '/>'
    form += '<input type="hidden" name="' + paramName + '" value="' + paramValue + '">'
    return form
        
