# Copyright 2012 Ben Kinslow
#    
# This file is part of ZFSDash.
#
# ZFSDash is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# ZFSDash is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with ZFSDash.  If not, see <http://www.gnu.org/licenses/>

import emailer
import logging
import cherrypy
import scheduler
import settings
import socket
import webber
import re

from subprocess import Popen, PIPE, STDOUT

debugPoolName = "DEBUG-Pool"
zpoolListDebugResult = debugPoolName + '   ONLINE  928G    580G    348G    62%     28'

zpoolStatusDebugResult =   ("  pool: pool\r\n"
                            " state: ONLINE\r\n"
                            " scan: scrub repaired 0 in 0h51m with 0 errors on Tue Dec  4 19:48:12 2012\r\n"
                            "config:\r\n"
                            "\r\n"
                            "    NAME                        STATE     READ WRITE CKSUM\r\n"
                            "    pool                        ONLINE       0     0     0\r\n"
                            "      raidz1-0                  ONLINE       0     0     0\r\n"
                            "        wwn-0x5000c5003f864210  ONLINE       0     0     0\r\n"
                            "        wwn-0x50014ee2b10fe557  ONLINE       0     0     0\r\n"
                            "        wwn-0x50014ee2027c938b  ONLINE       0     0     0\r\n"
                            "        wwn-0x50014ee1af68b8a9  ONLINE       0     0     0\r\n"
                            "\r\n"
                            "errors: No known data errors\r\n")

zpoolStatusDebugResult2 =  ("  pool: pool\r\n"
                            " state: ONLINE\r\n"
                            " scan: scrub in progress since Sat Dec  8 19:19:55 2012\r\n"
                            "   2.44G scanned out of 585G at 132M/s, 1h15m to go\r\n"
                            "   0 repaired, 33.0% done\r\n"
                            "config:\r\n"
                            "\r\n"
                            "    NAME                        STATE     READ WRITE CKSUM\r\n"
                            "    pool                        ONLINE       0     0     0\r\n"
                            "      raidz1-0                  ONLINE       0     0     0\r\n"
                            "        wwn-0x5000c5003f864210  ONLINE       0     0     0\r\n"
                            "        wwn-0x50014ee2b10fe557  ONLINE       0     0     0\r\n"
                            "        wwn-0x50014ee2027c938b  ONLINE       0     0     0\r\n"
                            "        wwn-0x50014ee1af68b8a9  ONLINE       0     0     0\r\n"
                            "\r\n"
                            "errors: No known data errors\r\n")



class ZpoolException(Exception):
    pass

class Zpool():
    class Health:
        DEGRADED = 'DEGRADED'
        FAULTED = 'FAULTED'
        OFFLINE = 'OFFLINE'
        ONLINE = 'ONLINE'
        REMOVED = 'REMOVED'
        UNAVAIL = 'UNAVAIL'
        INVALID = 'INVALID'

    def __init__(self, name):
        self.name = name
        self.health = self.Health.INVALID
        self.sizeStr = ''
        self.allocatedStr = ''
        self.freeStr = ''
        self.sizeGigaBytes = 0.0
        self.allocatedGigaBytes = 0.0
        self.freeGigaBytes = 0.0
        self.capacity = 100
        self.version = 'Unknown'
        self.warningState = False
        self.newWarning = False
        self.scanStatus = []
        self.configStatus = []
        self.errorStatus = []
        self.scrubbing = False
        self.scrubPercent = 0.0
        self.hostName = socket.gethostname()
        
    @staticmethod
    def convertSizeStrToGigabytes(sizeStr):
        result = 0.0
        if sizeStr.upper().endswith('G'):
            result = float(sizeStr[:-1])
        if sizeStr.upper().endswith('T'):
            result = float(sizeStr[:-1]) * 1024.0
        elif sizeStr.upper().endswith('M'):
            result = float(sizeStr[:-1]) / 1024.0
        elif sizeStr.upper().endswith('K'):
            result = float(sizeStr[:-1]) / (1024.0 * 1024.0)
        return result

    @staticmethod
    def formatBytes(gigabytes):
        if gigabytes > 1024.0:
            string = str(gigabytes/1024.0) + ' TB'
        elif gigabytes < 1.0:
            string = str(int(gigabytes*1024.0)) + ' kB'
        else:
            string = str(int(gigabytes)) + ' GB'            
        return string
    
    @staticmethod
    def convertPercentStrToInt(percentStr):
        result = 0
        if percentStr.endswith('%'):
            result = int(percentStr[:-1])
        return result
    
    @staticmethod
    def executeZpoolCommand(commandString, debugResult):
        try:
            logging.debug("Running command: " + commandString)
            p = Popen(commandString.split(), shell=False, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True)
            output = p.stdout.read().strip()
            logging.debug("Output from command: " + output)
        except Exception, e:
            logging.error("Could not execute command: " + commandString)
            logging.exception(e)
            output = debugResult
        return output
 
    @staticmethod
    def decodeZpoolStatusResult(commandResult):
        SCAN_TERM = "scan:"
        CONFIG_TERM = "config:"
        ERROR_TERM = "errors:"
        scanStatus = []
        configStatus = []
        errorStatus = []
        for line in commandResult.split("\n"):
            scanTermInLine = SCAN_TERM in line
            configTermInLine = CONFIG_TERM in line
            errorTermInLine = ERROR_TERM in line
            
            if scanTermInLine:
                # Add the first scan status line
                scanStatus.append(line.split(SCAN_TERM)[-1].strip())
            elif len(scanStatus) > 0  and len(configStatus) == 0 and not configTermInLine:
                # Add following scan lines
                scanStatus.append(line.strip())
            
            if configTermInLine:
                # Add the first config line
                configStatus.append(line.split(CONFIG_TERM)[-1].strip())
            elif len(configStatus) > 0 and len(errorStatus) == 0 and not errorTermInLine:
                # Add following scan lines
                configStatus.append(line.strip())
                
            if errorTermInLine:
                # Add the first error line
                errorStatus.append(line.split(ERROR_TERM)[-1].strip())
            elif len(errorStatus) > 0 and len(errorStatus) == 0 and not errorTermInLine:
                # Add following error lines
                errorStatus.append(line.strip())
                
        return scanStatus, configStatus, errorStatus

    def update(self):
        NAME_ATTRIB = 'name'
        HEALTH_ATTRIB = 'health'
        SIZE_ATTRIB = 'size'
        ALLOCATED_ATTRIB = 'allocated'
        FREE_ATTRIB = 'free'
        CAPACITY_ATTRIB = 'capacity'
        VERSION_ATTRIB = 'version'
        
        attribNameList = [NAME_ATTRIB, HEALTH_ATTRIB, SIZE_ATTRIB, ALLOCATED_ATTRIB, FREE_ATTRIB, CAPACITY_ATTRIB, VERSION_ATTRIB]
        cmd = 'zpool list -Ho ' + ','.join(attribNameList) + ' ' + self.name
        attribValueList = Zpool.executeZpoolCommand(cmd, zpoolListDebugResult).split()
        
        self.name = attribValueList[attribNameList.index(NAME_ATTRIB)]    
        self.health = attribValueList[attribNameList.index(HEALTH_ATTRIB)]     
        self.sizestr = attribValueList[attribNameList.index(SIZE_ATTRIB)]  
        self.allocatedStr = attribValueList[attribNameList.index(ALLOCATED_ATTRIB)]    
        self.freeStr = attribValueList[attribNameList.index(FREE_ATTRIB)]
        
        self.sizeGigaBytes = Zpool.convertSizeStrToGigabytes(self.sizestr)  
        self.allocatedGigaBytes = Zpool.convertSizeStrToGigabytes(self.allocatedStr)    
        self.freeGigaBytes = Zpool.convertSizeStrToGigabytes(self.freeStr)    
        self.capacity = Zpool.convertPercentStrToInt(attribValueList[attribNameList.index(CAPACITY_ATTRIB)])    

        self.version = attribValueList[attribNameList.index(VERSION_ATTRIB)]   

        if self.warningState == False:
            if self.health != self.Health.ONLINE:
                self.newWarning = True
                self.warningState = True
        else:
            self.newWarning = False
            self.warningState = (self.health != self.Health.ONLINE)
        
        cmd = 'zpool status ' + self.name
        zpoolStatusResult = Zpool.executeZpoolCommand(cmd, zpoolStatusDebugResult2)        
        self.scanStatus, self.configStatus, self.errorStatus = Zpool.decodeZpoolStatusResult(zpoolStatusResult)

        self.scrubbing = False
        for line in self.scanStatus:
            if line.find('scrub in progress') >= 0:
                self.scrubbing = True

            if self.scrubbing:
                match = re.compile(r', (\d+\.\d*)% done').search(line)
                if match:
                    self.scrubPercent = float(match.group(1))



    def getSummaryText(self):
        string = ''
        for row in self.getSummary():
            string += row[0] + ': ' + row[1] + '\r\n'
        logging.debug("Returning zpool summary: " + string)
        return string

    def getSummary(self):
        summary = []
        summary.append(['Host', self.hostName])
        summary.append(['Pool', self.name])
        summary.append(['Health', str(self.health)])
        summary.append(['Size', Zpool.formatBytes(self.sizeGigaBytes)])
        summary.append(['Allocated', Zpool.formatBytes(self.allocatedGigaBytes)])
        summary.append(['Free', Zpool.formatBytes(self.freeGigaBytes)])
        summary.append(['Capacity', str(self.capacity)+'%'])
        summary.append(['Version', str(self.version)])
        summary.append(['Scan Status', "\r\n".join(self.scanStatus)])
        summary.append(['Error Status', "\r\n".join(self.errorStatus)])
        
        #logging.debug("Returning zpool summary: " + "\r\n".join(summary))
        return summary
 
    def getScrubRow(self):
        if self.scrubbing:
            disableText = 'Disabled'
            descriptionText = 'Scrub is already in progress'
        else:
            disableText = ''
            descriptionText = 'Perform scrub'
        
        form = webber.getButtonWithParam('startScrub', 'Start Scrub', 'poolName', self.name, not self.scrubbing)
        scrubRow = [form, descriptionText]
        return scrubRow
    
    def getChart(self):
        return webber.getChart(self)
                
class ZpoolManager():
    def __init__(self):
        self.update()
        
        self.warningemailScheduler = None
        self.emailScheduler = None
        self.ipChecker = None

    def update(self):
        self.pools = []
        cmd = 'zpool list -Ho name'
        poolNamesList = Zpool.executeZpoolCommand(cmd, debugPoolName).split()         
        for poolName in poolNamesList:
            pool = Zpool(poolName)
            self.pools.append(pool)
            
    def startThreads(self):
        self.warningemailScheduler = scheduler.Scheduler(self.sendWarningEmails, runNow=True, period=settings.Schedule.warningEmailPeriod, threadName="WARNING Email Scheduler")
        self.emailScheduler = scheduler.Scheduler(self.sendStandardEmails, runNow=True, period=settings.Schedule.standardEmailPeriod, threadName="Email Scheduler")
   
    def stopThreads(self):
        self.warningemailScheduler.abort = True
        self.emailScheduler.abort = True

    def updatePools(self):
        for pool in self.pools:
            pool.update()

    def sendStandardEmails(self):
        self.updatePools()
        emailer.sendEmail(self.pools)
            
    def sendWarningEmails(self):
        self.updatePools()
        overallWarningState = False
        for pool in self.pools:
            overallWarningState = overallWarningState or pool.warningState 
        if overallWarningState:
            self.sendEmail(self.pools, warning=True)
            

