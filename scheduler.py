# Copyright 2012 Ben Kinslow
#    
# This file is part of ZFSDash.
#
# ZFSDash is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# ZFSDash is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with ZFSDash.  If not, see <http://www.gnu.org/licenses/>

import datetime
import time
import threading

class Scheduler:

    def __init__(self, method, period=datetime.timedelta(minutes=10), runNow=True, threadName="ScheduledThread"):

        if runNow:
            self.previousRunTime = datetime.datetime.fromordinal(1)
        else:
            self.previousRunTime = datetime.datetime.now()
            
        self.method = method
        self.period = period

        self.thread = None
        self.threadName = threadName
        self.abort = False
        
        self.thread = threading.Thread(None, self.runner, self.threadName)
        self.thread.start()

    def timeLeft(self):
        return self.period - (datetime.datetime.now() - self.previousRunTime)

    def runner(self):

        while True:
            if self.abort:
                self.abort = False
                self.thread = None
                return
            elif self.timeLeft() <= datetime.timedelta(microseconds=0):
                self.previousRunTime = datetime.datetime.now()
                self.method()

            time.sleep(1)
            
        self.thread = None